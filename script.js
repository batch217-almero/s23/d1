//console.log("Test connect");

// OBJECTS ------------------------------------------------------------------
// Used to represent real world objects

let	cellphone = {
	name: "Nokia 3210",
	manufacturedDate: 1999
}

console.log("Result from creating objects using initializers/literal notation:");
console.log(cellphone);
console.log(typeof cellphone);

// "this" inside an object in a function
function Laptop(name, manufacturedDate){
	this.name = name;
	this.manufacturedDate = manufacturedDate;
}

// create new object using function
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objectst using object constructors");
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating objectst using object constructors");
console.log(myLaptop);

// sample wrong statement since "new" keyword is not used
let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating objectst using object constructors");
console.log(oldLaptop);

// creating empty object
let computer = {};//sample 1
let myComputer = new Object(); //sample 2

// Accessing Object Properties -------------------------------------------------
// Using dot (.) notation
console.log("Result from dot notation: " + myLaptop.name);
console.log("Result from dot notation: " + myLaptop.manufacturedDate);

// Using square [] brackets
console.log("Result from square bracket notation: " + myLaptop['name']);
console.log("Result from square bracket notation: " + myLaptop['manufacturedDate']);


// Accessing Array Objects ----------------------------------------------------
let array = [laptop, myLaptop];

console.log(array[0]['name']); // first method
console.log(array[0].manufacturedDate); //second method


// Initialize, Add, Delete, Re-assign Object Properties------------------------

let car = {};
console.log(car);

// Initialize or Add property using dot (.) notation
car.name = "Honda Civic";
console.log("Result from adding property using dot notation");
console.log(car);

// Initialize using brackets [] notation
car['manufacturedDate'] = 2019;
console.log("Result from adding property using bracket notation");
console.log(car);

// Delete Object property
delete car['manufacturedDate']; //pwede din delete car.manufacturedDate;
console.log(car);

// Reassigning object properties
car.name = "Dodge Charger R/T";
console.log("Result from reassigning properties:")
console.log(car);


// OBJECT METHODS ---------------------------------------------------------------

// A function which is a property of an object
let person = {
	name: "Dhonel",
	talk: function(){
		console.log("Hello, my name is " + this.name);
	}
}

console.log(person);
console.log("Result from object method");
person.talk(); //invoke object method

// Add property with an object method/function
person.walk = function(){
	console.log(this.name + " walk 25 steps forward.");
};

person.walk();

// Another example

let friend = {
	firstName: "Dhonel",
	lastName: "Almero",
	//an object inside an object
	address: {
		city: "Masbate",
		country: "Philippines"
	},
	//nested array inside an object
	emails: ["dhonel15s@gmail.com", "dhonel.almero@bicol-u.edu.ph"],
	introduce: function(){
		console.log("Hello, my name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();

// Real Worldd Application of Objects -------------------------------------------------

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,

	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
	},

	faint: function(){
		console.log("pokemon fainted");
	}
}

console.log(myPokemon);

// Creating an object constructor to use on process "this" and "new"

function Pokemon(name, level){
	//properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// method function
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now to _targetPokemonHealth_");
	}

	this.faint = function(){
		console.log(this.name + " fainted");
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
